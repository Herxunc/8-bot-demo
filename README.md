# 8-bot-demo
Show case how to use 8 Parter-API to build a bot

# How to run
`npm run start`

# How to configure
See README under `manifest`

You are a vendor of 8 Partner-API.  Before launching this app you should
register your application with 8 Market Place.

Please contact `im8@no8.io` for instructions.
