# Here is how you setup your bot

## Vendor
You are a vendor of 8 Partner-API.  Before launching this app you should
register your application with 8 Market Place.

Store the following info in `vendor.json`
```json
{
  "id": "vendor-id-at-registration",
  "secret": "vendor-secret",
  "app": {
    "id": "echo-bot",
    "port": 3333,
    "callback": {
      "host": "https://your-app-domain.com/",
      "path": "callback_url"
    }
  },
  "api": {
    "endpoint": "https://partner-api.no8.io/"
  },
  "resource": {
    "giphy": {
      "apiKey": "your-giphy-api-key"
    }
  }
}
```

## Resource
For each provisioned resource id, create a config file `your-resource-id.json`
```json
{
  "id": "your-resource-id",
  "authorization": {
    "token": "Bearer authorization-token-issued-to-you"
  },
  "organization": {
    "id": "8-platform-organization"
  },
  "template": {
    "cache": {
    },
    "keys": [
      "template-id-1",
      "template-id-2"
    ]
  }
}
```
