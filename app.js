const Promise = require('bluebird'),
  retryp = require('promise-retry'),
  express = require('express'),
  basicAuth = require('express-basic-auth'),
  bodyJson = require('body-parser').json({ type: '*/*' }),
  requestp = require('request-promise'),
  url = require('url'),
  moment = require('moment');

const app = express();
const requireDir = require('require-dir');

const path = require('path');
const manifest = requireDir(path.resolve(process.argv[2]));

app.use(basicAuth(
  {
    users: {
      [manifest.vendor.id]: manifest.vendor.secret
    }
  }
));

function invokeApi(requestOpts) {
  const retryOpts = {
    retries: 3
  };
  return retryp((retry) => requestp(requestOpts).catch(retry), retryOpts).catch(error => {
    console.log(`invokeApi - error - ${error.message}`);
    throw error;
  });
}
function buildOpt(method, host, json = true) {
  const options = {
    method: method,
    uri: host,
    headers: undefined,
    body: undefined,
    json: json
  };
  console.log(method, host);
  return options;
}
function buildOpt8(method, path, resource, session) {
  const options = buildOpt(method, url.resolve(manifest.vendor.api.endpoint, path));
  options.headers = {
    Authorization: manifest[resource].authorization.token,
    'X-8-Partner-Api-Session': session.id
  };
  return options;
}
function buildTextMesssage(text) {
  return {
    contentType: "text/plain",
    data: {
      content: text
    }
  };
}
function buildImageMessage(image) {
  return {
    contentType: "application/x-image",
    data: {
      url: image
    }
  };
}
function buildTemplageMessage(resource, session, key) {
  let cache = (manifest[resource].template.cache || {});
  return Promise.resolve()
    .then(() => {
      if (cache[key] === undefined) {
        return invokeApi(buildOpt8('GET', `/classes/Template/${key}`, resource, session)).then(response => {
          cache[key] = response;
          return cache[key];
        });
      }
      return cache[key];
    })
    .then(template => {
      return {
        contentType: 'application/x-template',
        data: template.data,
        template: template.objectId
      };
    });
}
function sendText(resource, session, text) {
  const requestOpts = buildOpt8('POST', '/function/message', resource, session);
  requestOpts.body = {
    messages: [ buildTextMesssage(text) ],
    read: true
  };
  console.log(`reply - text - ${JSON.stringify(requestOpts, null, 2)}`);
  return invokeApi(requestOpts);
}
function sendImage(resource, session, imageUrl) {
  const requestOpts = buildOpt8('POST', '/function/message', resource, session);
  requestOpts.body = {
    messages: [ buildImageMessage(imageUrl)],
    read: true
  };
  console.log(`reply - image - ${JSON.stringify(requestOpts, null, 2)}`);
  return invokeApi(requestOpts);
}
function sendTemplate(resource, session, key) {
  return buildTemplageMessage(resource, session, key).then(data => {
    const requestOpts = buildOpt8('POST', '/function/message', resource, session);
    requestOpts.body = {
      messages: [ data ],
      read: true
    };
    console.log(`reply - template - ${JSON.stringify(requestOpts, null, 2)}`);
    return invokeApi(requestOpts);
  });
}
function sendMany(resource, session, messageArr) {
  const requestOpts = buildOpt8('POST', '/function/message', resource, session);
  requestOpts.body = {
    messages: [],
    read: true
  };
  messageArr.forEach(message => {
    if (message.text) {
      requestOpts.body.messages.push(
        buildTextMesssage(message.text)
      );
    } else if (message.image) {
      requestOpts.body.messages.push(
        buildImageMessage(message.image)
      );
    }
  });
  console.log(`reply - many - ${JSON.stringify(requestOpts, null, 2)}`);
  return invokeApi(requestOpts);
}
function schedule(resource, session, future) {
  const requestOpts = buildOpt8('POST', '/function/schedule', resource, session);
  const webhook = url.resolve(manifest.vendor.app.callback.host, manifest.vendor.app.callback.path);
  requestOpts.body = {
    callbackUrl: `${webhook}?resource=${resource}`,
    scheduleAt: future.toDate()
  };
  console.log(`schedule - ${JSON.stringify(requestOpts, null, 2)}`);
  return invokeApi(requestOpts);
}

function getGoofy({ contentType }) {
  return Promise.resolve()
    .then(() => {
      let text = "";
      switch (contentType) {
      default:
        return { text: "I don't understand this 😰 , Please send somthing else!" };
      case "application/x-image":
        return { text: `You sent me an image 🖼 !` };
      case "text/plain":
        const options = buildOpt('GET', 'http://more.handlino.com/sentences.json');
        options.qs = {
          n: 1,
          limit: '10,15'
        };
        return invokeApi(options)
          .then(resp => {
            return { text: resp.sentences.join('\n') };
          })
          .catch(error => {
            console.log(`goofy - error - ${error.message}`);
            return { text: `(burp)` };
          });
      }
    });
}

function getGiphy({ contentType, data }) {
  let query, apiKey = manifest.vendor.resource.giphy.apiKey;
  switch (contentType) {
  default:
    query = new Date().toString();
    break;
  case "text/plain":
    query = data.content;
    break;
  }
  const options = buildOpt('GET', 'https://api.giphy.com/v1/gifs/search');
  options.timeout = 1 * 1000;
  options.qs = {
    api_key: apiKey,
    q: query,
    limit: 10,
    rating: 'g'
  };
  return invokeApi(options)
    .then(response => {
      let idx = Math.floor(Math.random() * response.data.length);
      let image = response.data[idx].images.preview_gif;
      return { image: image.url };
    })
    .catch(error => {
      console.log(`giphy - error - ${error.message}`);
      return { text: '(slurp)' };
    });
}

const is_template = /send me a template|send template|send a template|template/i;
const is_notify = /(notify|remind) (me )?in (\d+)\s*(\w+)/i;

app.post('/event', bodyJson, (req, res) => {
  console.log(`event - ${JSON.stringify(req.body)}`);
  res.end('ok');
  Promise.each(req.body.records, requestJson => {
    let payload = requestJson.payload;
    if (payload.senderType === "_User") {
      return
    }
    if (payload.contentType === "application/x-read") {
      return
    }
    let resource = requestJson.identity.resource,
      session = requestJson.session;
    if (payload.contentType === "text/plain") {
      const text = payload.data.content;
      if (is_template.test(text)) {
        let templateKeys = manifest[resource].template.keys;
        let key = templateKeys[Math.floor(Math.random() * templateKeys.length)];
        return sendTemplate(resource, session, key);
      } else if (is_notify.test(text)) {
        let [ , , , num, unit ] = is_notify.exec(text);
        const duration = moment.duration(+(num), unit),
          future = moment().add(duration);
        return schedule(resource, session, future)
          .then(() => sendText(resource, session, `I will notify you in ${duration.as('minutes')} minutes`));
      } else {
        return Promise.join(getGiphy(payload), getGoofy(payload))
          .then(response => {
            return sendMany(resource, session, response);
          });
      }
    } else if (
      payload.contentType === "application/x-notify-event" &&
      payload.data.event === "event/customer-follow"
    ) {
      return sendText(resource, session, 'Welcome, have a biscuit 🍪 !');
    } else if (
      payload.contentType === "application/x-comment"
    ) {
      return; // noop
    } else {
      return Promise.join(getGiphy(payload), getGoofy(payload))
        .then(response => {
          return sendMany(resource, session, response);
        });
    }
  });
});

app.post(`/${manifest.vendor.app.callback.path}`, bodyJson, (req, res) => {
  console.log(`event - ${JSON.stringify(req.query)} - ${JSON.stringify(req.body)}`);
  res.end('ok');

  const resource = req.query.resource,
    session = req.body.session;

  Promise.resolve()
    .then(() => {
      const response = [
        {
          text: `It is ${new Date}`,
        },
        {
          text: "I woke up from sleep to notify you!",
        }
      ];
      return sendMany(resource, session, response);
    });
});

app.listen(manifest.vendor.app.port, () => {
  console.log(`Echo Bot listening on port ${manifest.vendor.app.port}!`);
});
